# ASCIII

Asciii is a work in progress framework made to build ascii games or applications in lua.
It is free, open-source and made with love.
My aim is, of course, to make it work on Windows, Mac OS X, Linux (and perhaps more).

# BUILD

To build Asciii, you need 2 external libraries, Raylib and Lua

raylib:
	version: 3.5.0
	url: https://github.com/raysan5/raylib/releases/tag/3.5.0
	download as: this-asciii-directory/lib/raylib/
	details: os dependant, you must download the corresponding library (depending on the version and your operating system). Nothing to do once downloaded.

lua:
	version: 5.4.2
	url: https://www.lua.org/versions.html
	download as: this-asciii-directory/lib/lua/
	details: os independent, simply use "make" to build the library

Once you downloaded the 2 libraries, you can simply run the build.sh script.
It will build the personal library I made and use and then build Asciii.

# API

? [ ] presetcharacters(characters)
? [ ] presetautorecord(length)			if length == 0, no auto recording
[x] presetlinespacing(percent)
[x] presetcharspacing(percent)
[x] presetfont(fontpath)
[x] presetmargin(width)
[x] setwindow(cw, ch, fontsize)
[x] setwindow(cw, ch, pw, ph)
 
[x] getwidth()
[x] getheight()

? [ ] setgradient(string)

? What if we want less characters for the gradient ?
? What about compressed image when we want less characters ?

[x] newcanvas([w, h])					returns a new canvas (default: active canvas dimensions)
[ ] newcanvascimg(path)					returns a new canvas from a compressed ascii image
[x] setcanvas([canvas])					set active canvas (default: reset active canvas to main)
[x] putcanvas(canvas)					draw passed canvas to active canvas
[x] maskcanvas(canvas, maskcanvas)		mask canvas with maskcanvas

[ ] setbackground(r, g, b)				set background color
[ ] setforeground(r, g, b)				set foreground color (text)
[x] clear()								clear active canvas

[x] putc(x, y, char)					put a char (from a lua string)
[x] putci(x, y, index)				put a char (from its index on gradient list)
[x] putca(x, y, index)				put a char (from its index on ascii list)

[x] getc(x, y)							get cell character value
[x] getci(x, y)							get cell character index value
[x] getca(x, y)							get cell character ascii value

[x] ctoi()								char to index
[x] ctoa()								char to ascii
[x] itoc()								index to char
[x] itoa()								index to ascii
[x] atoc()								ascii to char
[x] atoi()								ascii to index

[ ] text(x, y, text[, w])
[ ] textmode()
[ ] textalign()
[ ] textwrap()

[x] setc(c)											set default char to draw
[x] setci(index)
[x] setca(index)
[x] line(x1, y1, x2, y2[, c])						draw a line
[x] rect(x, y, w, h[, c])							draw a rectangle
[x] rectf(x, y, w, h[, c])							draw a filled rectangle
[x] circle(x, y, r, h[, c])							draw a circle
[x] circlef(x, y, r, h[, c])						draw a filled circle
[x] poly(x, y, w, h, vertices[, c])					draw a regular polygon
[ ] polyf(x, y, w, h, vertices[, c])				draw a filled regular polygon
[x] cpoly(x, y, ew, eh, iw, ih, extvertices[, c])	draw a regular concave polygon
[ ] cpolyf(x, y, ew, eh, iw, ih, extvertices[, c])	draw a filled regular concave polygon

[x] fill(x, y[, c])

[x] repc(cfrom, cto)								replace char to another on canvas
[x] repci(cifrom, cito)
[x] repca(cafrom, cato)

[ ] box(x, y, w, h)
[ ] setbox(characters)
? [ ] beginboxes()
? [ ] endboxes()

[ ] linefunc(x1, y1, x2, y2)			call function on line

[ ] newimg(path)						new image from path
[ ] getimg(image, x, y)					get image pixel color
[ ] img:tocanvas(width, height, "mode")	mode = "picture" or "value""
? [ ] putimg(image, x, y, xrec, yrec, wrec, hrec[, advanced])	translate image to ascii

- - - record

[ ] startrecording()					start buffered screen recording
[ ] saverecording(path)					save buffered screen recording to png sequence
[ ] saveshot(path)						save shot of the screen or of the passed canvas
[ ] savecompressed(path)				save canvas to compressed image (.png)
[ ] savecompressed(path, data)			save canvas to compressed image with additional data (text ?) chunks

- - - animation ?

[x] wiggle(range, duration)				bounce on 'range' on 'duration' (sin(time()))

