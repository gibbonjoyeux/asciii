#!/usr/bin/env bash
############################### BY GIBBONJOYEUX ################################

################################################################################
### FUNCTIONS
################################################################################

################################################################################
### MAIN
################################################################################

# LOAD SUBMODULES
git submodule init
git submodule update --remote --merge

# BUILD C-LIB
cd lib/c-lib
./build.sh
cd -

# BUILD ASCIII OBJECTS
gcc src/*.c \
	-I inc \
	-I lib/c-lib/inc \
	-I lib/c-lib/lib/c-linked-list/inc \
	-I lib/c-lib/lib/c-hash-table/inc \
	-I lib/c-lib/lib/c-binary-tree/inc \
	-I lib/c-lib/lib/c-string/inc \
	-I lib/lua \
	-I lib/raylib/include \
	-c

# BUILD ASCII
gcc -o asciii *.o \
	lib/c-lib/libgb.a \
	lib/lua/liblua.a \
	-framework CoreVideo \
	-framework IOKit \
	-framework Cocoa \
	-framework GLUT \
	-framework OpenGL \
	lib/raylib/lib/libraylib.a

rm *.o
