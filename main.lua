--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- test.lua

--[[
--
-- 
--
--]]

function		init()
	print("init() function")
	setcharspacing(70)
	setfont("res/fonts/velvetyne/TINY5x3-80.otf")
	setwindow(65, 25, 800, 480)
	--setwindow(80, 25, 20)
	--cnv = newcanvas(getwidth(), getheight())
	--print(cnv:getwidth(), getwidth())
end

function		update()
	local	x, y

	clear();
	for y=0,getheight()-1 do
		for x=0,getwidth()-1 do
			putci(x, y, x)
		end
	end
	putc(1,2,"S")
	putc(2,2,"E")
	putc(3,2,"B")
end

function		draw()
end
