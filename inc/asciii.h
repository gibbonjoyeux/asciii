/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef ASCIII_H
# define ASCIII_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include "libgb.h"
#include "raylib.h"

#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <png.h>

#include <stdio.h>
#include <math.h>

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

#define CHARS_ASCII				" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
#define CHARS_LEN				(sizeof(CHARS_ASCII) - 1)

#define	DEFAULT_CHAR			'.'
#define DEFAULT_FPS				60
#define	DEFAULT_FONT			"res/fonts/classic/courier-new.ttf"
#define DEFAULT_RECORD_LEN		(DEFAULT_FPS * 10)

#define PI2						(2.0 * PI)

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////

typedef	struct t_asciii		t_asciii;
typedef	struct t_canvas		t_canvas;

////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////

struct				t_asciii {
	/// CHARACTERS
	struct {
		char		ascii[CHARS_LEN];
		char		gradient[CHARS_LEN];
		char		reverse_gradient[CHARS_LEN];
		char		set;
		char		active;
	}				chars;
	/// FONT
	struct {
		Font		font;
		int			size;
		int			width;
		int			height;
		int			charspacing;
		int			linespacing;
		float		ratio;
		char		*path;
	}				font;
	/// FORMAT
	struct {
		int			w;
		int			h;
		int			screen_w;
		int			screen_h;
		int			margin;
		int			offset_x;
		int			offset_y;
	}				format;
	/// CANVAS
	struct	{
		t_canvas	*base;
		t_canvas	*set;
		t_canvas	*active;
	}				canvas;
	/// RECORD
	struct	{
		bool		auto_record;
		char		***buffer;
		int			index;
		int			saved;
		int			length;
	}				record;
};

struct				t_canvas {
	int				width;
	int				height;
	char			**array;
};

t_asciii			asciii;
lua_State			*l;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

bool		is_on_canvas(int x, int y);

int			get_param_canvas(void);
char		get_param_char(int index);
int64_t		get_param_int(int index);
double		get_param_float(int index);

int			compute_font_gradient(void);

void		render_array(char **array);
void		record_update(void);
void		record_save(void);

void		draw_char(t_canvas *canvas, char c, int x, int y);
void		draw_line(t_canvas *canvas, char c, int x0, int x1, int y0, int y1);
void		draw_horizontal_line(t_canvas *canvas, char c, int y, int x0,int x1);
void		draw_vertical_line(t_canvas *canvas, char c, int x, int y0, int y1);
void		draw_rect(t_canvas *canvas, char c, int x, int y, int w, int h);
void		draw_rectf(t_canvas *canvas, char c, int x, int y, int w, int h);
void		draw_circ(t_canvas *canvas, char c, int x, int y, int rw, int rh);
void		draw_circf(t_canvas *canvas, char c, int x, int y, int rw, int rh);
void		draw_poly(t_canvas *canvas, char c, int x, int y, int w, int h,
			int vertices, double offset);
void		draw_cpoly(t_canvas *canvas, char c, int x, int y, int ext_w,
			int ext_h, int int_w, int int_h, int external_vertices,
			double offset);
void		draw_fill(t_canvas *canvas, char c, int x, int y);
void		draw_replace(t_canvas *canvas, char from, char to);

//////////////////////////////////////////////////
/// API FUNCTIONS
//////////////////////////////////////////////////

void		init_global_functions(void);
void		init_meta_canvas(void);

//////////////////////////////
/// API WINDOW
//////////////////////////////
int			api_window_setwindow(lua_State *l);
int			api_window_setcharspacing(lua_State *l);
int			api_window_setlinespacing(lua_State *l);
int			api_window_setmargin(lua_State *l);
int			api_window_setfont(lua_State *l);

//////////////////////////////
/// API CANVAS
//////////////////////////////
int			api_canvas_garbage(lua_State *l);

int			api_canvas_newcanvas(lua_State *l);
int			api_canvas_setcanvas(lua_State *l);
int			api_canvas_putcanvas(lua_State *l);
int			api_canvas_maskcanvas(lua_State *l);

int			api_canvas_clear(lua_State *l);

int			api_canvas_getwidth(lua_State *l);
int			api_canvas_getheight(lua_State *l);

int			api_canvas_line(lua_State *l);
int			api_canvas_rect(lua_State *l);
int			api_canvas_rectf(lua_State *l);
int			api_canvas_circ(lua_State *l);
int			api_canvas_circf(lua_State *l);
int			api_canvas_poly(lua_State *l);
int			api_canvas_cpoly(lua_State *l);
int			api_canvas_fill(lua_State *l);

int			api_canvas_putc(lua_State *l);
int			api_canvas_putci(lua_State *l);
int			api_canvas_putca(lua_State *l);
int			api_canvas_getc(lua_State *l);
int			api_canvas_getci(lua_State *l);
int			api_canvas_getca(lua_State *l);

int			api_canvas_repc(lua_State *l);
int			api_canvas_repci(lua_State *l);
int			api_canvas_repca(lua_State *l);

//////////////////////////////
/// API GLOBAL
//////////////////////////////
int			api_global_ctoi(lua_State *l);
int			api_global_ctoa(lua_State *l);
int			api_global_itoc(lua_State *l);
int			api_global_itoa(lua_State *l);
int			api_global_atoc(lua_State *l);
int			api_global_atoi(lua_State *l);

int			api_global_setc(lua_State *l);
int			api_global_setci(lua_State *l);
int			api_global_setca(lua_State *l);

int			api_global_wiggle(lua_State *l);

#endif
