/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		get_params(t_canvas **mask, t_canvas **dst, int *x, int *y,
				int *rx, int *ry, int *rw, int *rh) {
	int			off;

	/// CANVAS
	off = 0;
	if (lua_type(l, 2) == LUA_TUSERDATA) {
		*dst = lua_touserdata(l, 1);
		off = 1;
	} else {
		*dst = asciii.canvas.set;
	}
	*mask = lua_touserdata(l, 1 + off);
	/// COORDS
	*x = get_param_int(2 + off);
	*y = get_param_int(3 + off);
	*rx = get_param_int(4 + off);
	*ry = get_param_int(5 + off);
	*rw = (*mask)->width;
	*rh = (*mask)->height;
	if (lua_type(l, 6 + off) == LUA_TNUMBER) {
		*rw = MIN(*rw, get_param_int(6 + off));
		if (lua_type(l, 7 + off) == LUA_TNUMBER)
			*rh = MIN(*rh, get_param_int(7 + off));
	}
	/// CHECK
	if (*x < 0) {
		*rx -= *x;
		*rw += *x;
	}
	if (*y < 0) {
		*ry -= *y;
		*rh += *y;
	}
	if (*x + *rw > (*dst)->width)
		*rw = (*dst)->width - *x;
	if (*y + *rh > (*dst)->height)
		*rh = (*dst)->height - *y;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int				api_canvas_maskcanvas(lua_State *l) {
	t_canvas	*mask, *dst;
	int			dx, dy;
	int			x, y;
	int			rx, ry, rw, rh;

	l = NULL;
	get_params(&mask, &dst, &x, &y, &rx, &ry, &rw, &rh);
	if (rw < 0 && rh < 0)
		return 0;
	if (mask == dst)
		return 0;
	for (dy = 0; dy < rh; ++dy)
		for (dx = 0; dx < rw; ++dx)
			if (mask->array[y + dy][x + dx] == ' ')
				dst->array[y + dy][x + dx] = ' ';
	return 0;
}
