/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		init_asciii() {
	int			x, y;
	char		**array;

	/// CANVAS
	asciii.canvas.base = malloc(sizeof(t_canvas));
	asciii.canvas.base->width = asciii.format.w;
	asciii.canvas.base->height = asciii.format.h;
	if (asciii.canvas.base == NULL)
		gb_exit_fail("Not enough memory to init asciii");
	array = (char**)gb_arrnew(asciii.format.w, asciii.format.h,
	sizeof(char));
	if (array == NULL)
		gb_exit_fail("Not enough memory to init asciii");
	for (y = 0; y < asciii.format.h; ++y) {
		for (x = 0; x < asciii.format.w; ++x)
			array[y][x] = ' ';
		array[y][x] = 0;
	}
	//// SET CANVAS
	asciii.canvas.base->array = array;
	asciii.canvas.set = asciii.canvas.base;
	asciii.canvas.active = NULL;
	/// RECORDING
	asciii.record.buffer = gb_arrmnew(sizeof(char), 3, DEFAULT_RECORD_LEN,
	asciii.format.h, asciii.format.w);
	if (asciii.record.buffer == NULL)
		gb_exit_fail("Not enough memory to init asciii");
	asciii.record.length = DEFAULT_RECORD_LEN;
	asciii.record.index = 0;
	asciii.record.saved = 0;
	asciii.record.auto_record = true;
}

static void		init_raylib(void) {
	SetWindowSize(asciii.format.screen_w, asciii.format.screen_h);
	SetTargetFPS(DEFAULT_FPS);
	/// CENTER WINDOW
	SetWindowPosition((GetMonitorWidth(0) - asciii.format.screen_w) / 2,
	(GetMonitorHeight(0) - asciii.format.screen_h) / 2);
}

static int		compute_font_width() {
	char		str[2] = {0, 0};
	Vector2		dim;
	int			i;
	float		max;

	max = 0;
	for (i = 0; i < (int)CHARS_LEN; ++i) {
		str[0] = asciii.chars.ascii[i];
		dim = MeasureTextEx(asciii.font.font, str, asciii.font.size, 0);
		max = MAX(max, dim.x);
	}
	return max;
}

static void		compute_offset() {
	asciii.format.offset_x = asciii.format.offset_x + (asciii.format.screen_w
	- (asciii.format.w * asciii.font.width)) / 2;
	asciii.format.offset_y = asciii.format.offset_y + (asciii.format.screen_h
	- (asciii.format.h * asciii.font.height)) / 2;
	printf("%d %d\n", asciii.format.offset_x, asciii.format.offset_y);
}

static void	mode_font() {
	/// PARAMS
	asciii.format.w = lua_tointeger(l, 1);
	asciii.format.h = lua_tointeger(l, 2);
	asciii.font.size = lua_tointeger(l, 3);
	/// FONT
	asciii.font.font = LoadFontEx(asciii.font.path, asciii.font.size, NULL,
	255);
	asciii.font.width = (compute_font_width() * asciii.font.charspacing) / 100;
	asciii.font.height = (asciii.font.size * asciii.font.linespacing) / 100;
	/// COMPUTE SCREEN DIMENSIONS
	asciii.format.screen_w = asciii.format.w * asciii.font.width
	+ asciii.format.margin * 2;
	asciii.format.screen_h = asciii.format.h * asciii.font.height
	+ asciii.format.margin * 2;
}

static void	mode_pixel() {
	double	percent;
	double	ratio;
	int		width, height;

	/// PARAMS
	asciii.format.w = lua_tointeger(l, 1);
	asciii.format.h = lua_tointeger(l, 2);
	asciii.format.screen_w = lua_tointeger(l, 3);
	asciii.format.screen_h = lua_tointeger(l, 4);
	/// FONT
	width = asciii.format.screen_w - asciii.format.margin * 2;
	height = asciii.format.screen_h - asciii.format.margin * 2;
	percent = (double)asciii.font.linespacing / (double)100.0;
	asciii.font.size = (int)((double)(height / asciii.format.h) / percent);
	asciii.font.font = LoadFontEx(asciii.font.path, asciii.font.size, NULL,
	255);
	asciii.font.width = (compute_font_width() * asciii.font.charspacing) / 100;
	asciii.font.height = (asciii.font.size * asciii.font.linespacing) / 100;
	/// FONT DOESNT FIT
	ratio = (double)width / (double)(asciii.format.w * asciii.font.width + 2
	* asciii.format.margin);
	if (ratio < 1) {
		asciii.font.size *= ratio;
		UnloadFont(asciii.font.font);
		asciii.font.font = LoadFontEx(asciii.font.path, asciii.font.size, NULL,
		255);
		asciii.font.width = (compute_font_width() * asciii.font.charspacing) / 100;
		asciii.font.height = (asciii.font.size * asciii.font.linespacing) / 100;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			api_window_setwindow(lua_State *l) {
	if (lua_type(l, 4) == LUA_TNUMBER)
		mode_pixel();
	else
		mode_font();
	compute_offset();
	init_asciii();
	init_raylib();
	return 0;
}
