/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	push_function(char *name, lua_CFunction func) {
	lua_pushstring(l, name);
	lua_pushcfunction(l, func);
	lua_settable(l, -3);
}

static void	push_functions(void) {
	push_function("getwidth", api_canvas_getwidth);
	push_function("getheight", api_canvas_getheight);

	push_function("getc", api_canvas_getc);
	push_function("getci", api_canvas_getci);
	push_function("getca", api_canvas_getca);

	push_function("putc", api_canvas_putc);
	push_function("putci", api_canvas_putci);
	push_function("putca", api_canvas_putca);

	push_function("line", api_canvas_line);
	push_function("rect", api_canvas_rect);
	push_function("rectf", api_canvas_rectf);
	push_function("circ", api_canvas_circ);
	push_function("circf", api_canvas_circf);
	push_function("poly", api_canvas_poly);
	push_function("cpoly", api_canvas_cpoly);
	push_function("fill", api_canvas_fill);

	push_function("putcanvas", api_canvas_putcanvas);
	push_function("maskcanvas", api_canvas_maskcanvas);

	push_function("clear", api_canvas_clear);

	push_function("repc", api_canvas_repc);
	push_function("repci", api_canvas_repci);
	push_function("repca", api_canvas_repca);

	push_function("__gc", api_canvas_garbage);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		init_meta_canvas(void) {
	/// CREATE TABLE
	lua_newtable(l);
	//// PUT METHODS
	push_functions();
	//// PUSH TABLE
	lua_setglobal(l, "__asciii_meta_canvas");
	/// SET CREATED TABLE AS METATABLE
	lua_getglobal(l, "__asciii_meta_canvas");
	lua_pushstring(l, "__index");
	lua_getglobal(l, "__asciii_meta_canvas");
	lua_settable(l, -3);
	lua_pop(l, 1);
}
