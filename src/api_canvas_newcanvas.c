/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int				api_canvas_newcanvas(lua_State *l) {
	t_canvas	*canvas;
	int			w, h;

	w = lua_tointeger(l, 1);
	h = lua_tointeger(l, 2);
	/// CREATE CANVAS OBJECT
	canvas = lua_newuserdata(l, sizeof(t_canvas));
	if (canvas == NULL)
		gb_exit_fail("Not enough memory to create canvas");
	canvas->width = w;
	canvas->height = h;
	canvas->array = (char**)gb_arrnew(w, h, sizeof(char));
	if (canvas->array == NULL)
		gb_exit_fail("Not enough memory to create canvas");
	/// SET METATABLE
	lua_getglobal(l, "__asciii_meta_canvas");
	lua_setmetatable(l, -2);
	printf("new canvas created\n");
	return 1;
}

