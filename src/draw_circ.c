/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	point(t_canvas *canvas, char c, int x, int y, int offx, int offy) {
	draw_char(canvas, c, x - offx, y - offy);
	draw_char(canvas, c, x + offx, y - offy);
	draw_char(canvas, c, x - offx, y + offy);
	draw_char(canvas, c, x + offx, y + offy);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		draw_circ(t_canvas *canvas, char c, int x, int y, int rw, int rh) {
	int		w;
	double	h;
	double	i;
	int		last_w;

	if (EVEN(rw) == true)
		rw -= 1;
	if (ODD(rh) == true)
		rh -= 1;
	h = (double)rh / 2;
	last_w = 0;
	for (i = h; i >= 0; --i) {
		w = cos(asin(i / h)) * ((double)rw / 2);
		point(canvas, c, x, y, w, i);
		while (w - last_w > 1) {
			last_w += 1;
			draw_char(canvas, c, x - last_w, y - i);
			draw_char(canvas, c, x + last_w, y - i);
			draw_char(canvas, c, x - last_w, y + i);
			draw_char(canvas, c, x + last_w, y + i);
		}
		last_w = w;
	}
}
