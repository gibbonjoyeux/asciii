/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	constrain_rect(t_canvas *canvas, int *x, int *y, int *w, int *h) {
	if (*x < 0) {
		*w += *x;
		*x = 0;		
	}
	if (*y < 0) {
		*h += *y;
		*y = 0;
	}
	if (*x + *w > canvas->width)
		*w = canvas->width - *x;
	if (*y + *h > canvas->height)
		*h = canvas->height - *y;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		draw_rectf(t_canvas *canvas, char c, int rx, int ry, int rw,
			int rh) {
	char	*line;
	int		x, y;

	constrain_rect(canvas, &rx, &ry, &rw, &rh);
	for (y = 0; y < rh; ++y) {
		line = canvas->array[y + ry];
		for (x = 0; x < rw; ++x) {
			line[x + rx] = c;
		}
	}
}
