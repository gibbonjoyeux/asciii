/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			save_png(Image img, char *path, png_byte **rows_pointers) {
	FILE			*f;
	png_structp		png_ptr;
	png_infop		info_ptr;
	int				i;

	/// CREATE FILE
	f = fopen(path, "wb");
	if (f == NULL)
		return;
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,
	NULL, NULL);
	if (png_ptr == NULL) {
		fclose(f);
		return;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
		fclose(f);
		return;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_write_struct(&png_ptr, &info_ptr);
		fclose(f);
		return;
	}
	/// INIT IMAGE
	png_set_filter(png_ptr, 0, PNG_FILTER_NONE /*| PNG_VALUE_NONE*/);
	png_set_compression_level(png_ptr, 1 /*Z_BEST_SPEED*/);
	png_set_IHDR(png_ptr, info_ptr, img.width, img.height,
	8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
	PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	/* ZONE FOR UNKNOWN CHUNKS */
	/// CREATE IMAGE
	for (i = 0; i < img.height; ++i)
		rows_pointers[img.height - i - 1] =
		(png_byte*)&(((int32_t*)img.data)[img.width * i]);
	png_init_io(png_ptr, f);
	png_set_rows (png_ptr, info_ptr, rows_pointers);
	/// EXPORT IMAGE
	png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
	png_write_end(png_ptr, info_ptr);
	png_destroy_write_struct(&png_ptr, &info_ptr);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void				record_save() {
	char			path[256];
	Image			render_img;
	RenderTexture2D	render;
	int				i;
	png_byte		**rows_pointers;

	/// DRAW WAITING MESSAGE
	BeginDrawing();
		DrawRectangle(0, 0, asciii.format.screen_w, asciii.format.screen_h,
		(Color){255, 255, 255, 150});
		DrawText("Saving record...", 10, 12, 20, BLACK);
		DrawText("Saving record...", 10, 10, 20, WHITE);
	EndDrawing();
	/// CREATE RENDER
	render = LoadRenderTexture(asciii.format.screen_w, asciii.format.screen_h);
	/// COMPUTE FRAMES
	rows_pointers = NULL;
	for (i = 0; i < asciii.record.saved; ++i) {
		/// DRAW FRAME
		BeginTextureMode(render);
			render_array(asciii.record.buffer[asciii.record.index]);
		EndTextureMode();
		/// EXPORT FRAME
		render_img = GetTextureData(render.texture);
		if (rows_pointers == NULL) {
			rows_pointers = calloc(render_img.height, sizeof(png_byte*));
			if (rows_pointers == NULL) {
				UnloadImage(render_img);
				UnloadRenderTexture(render);
				return;
			}
		}
		sprintf(path, "output/%.5d.png", i);
		save_png(render_img, path, rows_pointers);
		UnloadImage(render_img);
		/// UPDATE INDEX
		if (asciii.record.index == 0) {
			asciii.record.index = asciii.record.length - 1;
		} else {
			asciii.record.index -= 1;
		}
	}
	/// FREE
	gb_memdel((void**)&rows_pointers);
	UnloadRenderTexture(render);
}
