/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		record_update() {
	char	**buffer;
	int		x, y;

	if (asciii.record.auto_record == false)
		return;
	/// COPY CANVAS TO RECORD BUFFER
	buffer = asciii.record.buffer[asciii.record.index];
	for (y = 0; y < asciii.format.h; ++y) {
		for (x = 0; x < asciii.format.w; ++x) {
			buffer[y][x] = asciii.canvas.base->array[y][x];
		}
	}
	/// SAVE RECORD
	if (IsKeyPressed(KEY_F9) == true)
		record_save();
	/// UPDATE RECORD
	asciii.record.index += 1;
	if (asciii.record.index >= asciii.record.length)
		asciii.record.index = 0;
	asciii.record.saved = MIN(asciii.record.saved + 1, asciii.record.length);
}
