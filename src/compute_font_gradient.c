/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

typedef struct {
	char	c;
	int		value;
} t_char;

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static int		char_comp(void *c1, void *c2) {
	return ((t_char*)c1)->value - ((t_char*)c2)->value;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			compute_font_gradient() {
	t_char	chars[CHARS_LEN];
	char	char_str[2] = {0, 0};
	Image	img;
	Color	color;
	Vector2	vec;
	float	font_size;
	int		count;
	int		x, y;
	int		c;

	font_size = asciii.font.size;
	for (c = 0; c < (int)CHARS_LEN; c += 1) {
		char_str[0] = asciii.chars.ascii[c];
		vec = MeasureTextEx(asciii.font.font, char_str, font_size, 0);
		img = ImageTextEx(asciii.font.font, char_str, font_size, 0, BLACK);
		count = 0;
		for (y = 0; y < img.height; ++y) {
			for (x = 0; x < img.width; ++x) {
				color = GetPixelColor(&((int*)img.data)[y * img.width + x], img.format);
				//count += color.a;
				if (color.a > 0)
					count += 1;
			}
		}
		chars[c].c = asciii.chars.ascii[c];
		chars[c].value = count;
		UnloadImage(img);
	}
	gb_sort(&(chars[0]), CHARS_LEN, sizeof(t_char), char_comp);
	for (c = 0; c < (int)CHARS_LEN; ++c) {
		asciii.chars.gradient[c] = chars[c].c;
		asciii.chars.reverse_gradient[chars[c].c - ' '] = c;
	}
	return 0;
}
