/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		draw_cpoly(t_canvas *canvas, char c, int px, int py, int ew, int eh,
			int iw, int ih, int external_vertices, double offset) {
	double	angle;
	int		vertices;
	int		w, h;
	int		x, y;
	int		last_x, last_y;
	int		i;

	vertices = external_vertices * 2;
	for (i = 0; i <= vertices; ++i) {
		if (i & 1) {
			w = ew;
			h = eh;
		} else {
			w = iw;
			h = ih;
		}
		angle = (double)i * (PI2 / (double)vertices);
		x = px + (int)(cos(angle + offset * PI2) * w);
		y = py + (int)(sin(angle + offset * PI2) * h);
		if (i > 0)
			draw_line(canvas, c, last_x, last_y, x, y);
		last_x = x;
		last_y = y;
	}
}
