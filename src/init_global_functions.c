/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		init_global_functions(void) {
	/// WINDOW
	lua_register(l, "setwindow", api_window_setwindow);
	lua_register(l, "setcharspacing", api_window_setcharspacing);
	lua_register(l, "setlinespacing", api_window_setlinespacing);
	lua_register(l, "setmargin", api_window_setmargin);
	lua_register(l, "setfont", api_window_setfont);

	/// CANVAS
	lua_register(l, "newcanvas", api_canvas_newcanvas);
	lua_register(l, "setcanvas", api_canvas_setcanvas);
	lua_register(l, "putcanvas", api_canvas_putcanvas);
	lua_register(l, "maskcanvas", api_canvas_maskcanvas);

	lua_register(l, "getwidth", api_canvas_getwidth);
	lua_register(l, "getheight", api_canvas_getheight);

	lua_register(l, "line", api_canvas_line);
	lua_register(l, "rect", api_canvas_rect);
	lua_register(l, "rectf", api_canvas_rectf);
	lua_register(l, "circ", api_canvas_circ);
	lua_register(l, "circf", api_canvas_circf);
	lua_register(l, "poly", api_canvas_poly);
	lua_register(l, "cpoly", api_canvas_cpoly);
	lua_register(l, "fill", api_canvas_fill);

	lua_register(l, "getc", api_canvas_getc);
	lua_register(l, "getci", api_canvas_getci);
	lua_register(l, "getca", api_canvas_getca);

	lua_register(l, "putc", api_canvas_putc);
	lua_register(l, "putci", api_canvas_putci);
	lua_register(l, "putca", api_canvas_putca);

	lua_register(l, "repc", api_canvas_repc);
	lua_register(l, "repci", api_canvas_repci);
	lua_register(l, "repca", api_canvas_repca);

	lua_register(l, "clear", api_canvas_clear);

	/// GLOBAL
	lua_register(l, "setc", api_global_setc);
	lua_register(l, "setci", api_global_setci);
	lua_register(l, "setca", api_global_setca);

	lua_register(l, "ctoi", api_global_ctoi);
	lua_register(l, "ctoa", api_global_ctoa);
	lua_register(l, "itoc", api_global_itoc);
	lua_register(l, "itoa", api_global_itoa);
	lua_register(l, "atoc", api_global_atoc);
	lua_register(l, "atoi", api_global_atoi);

	lua_register(l, "wiggle", api_global_wiggle);
}
