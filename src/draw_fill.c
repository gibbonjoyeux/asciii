/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static int	fill_left(t_canvas *canvas, char c, char to_fill, int x, int y) {
	int		min_x;

	min_x = x;
	while (x >= 0 && canvas->array[y][x] == to_fill) {
		canvas->array[y][x] = c;
		x -= 1;
		min_x -= 1;
	}
	return min_x + 1;
}

static int	fill_right(t_canvas *canvas, char c, char to_fill, int x, int y) {
	int		max_x;

	max_x = x;
	while (x < canvas->width - 1 && canvas->array[y][x + 1] == to_fill) {
		canvas->array[y][x + 1] = c;
		x += 1;
		max_x += 1;
	}
	return max_x - 1;
}

static void	recursive_top_down(t_canvas *canvas, char c, char to_fill,
			int min_x, int max_x, int y) {
	int		x;

	for (x = min_x; x < max_x; x++) {
		/// UP
		if (y > 0) {
			if (to_fill == canvas->array[y - 1][x])
				draw_fill(canvas, c, x, y - 1);
		}
		/// DOWN
		if (y < canvas->height - 1) {
			if (to_fill == canvas->array[y + 1][x])
				draw_fill(canvas, c, x, y + 1);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		draw_fill(t_canvas *canvas, char c, int x, int y) {
	char	to_fill;
	int		min_x, max_x;

	if (x < 0 || x >= canvas->width
	|| y < 0 || y >= canvas->height)
		return;
	to_fill = canvas->array[y][x];
	if (to_fill == c)
		return;
	min_x = fill_left(canvas, c, to_fill, x, y);
	max_x = fill_right(canvas, c, to_fill, x, y);
	recursive_top_down(canvas, c, to_fill, min_x, max_x, y);
}
