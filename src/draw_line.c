/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	line_low(t_canvas *canvas, char c, int x0, int y0, int x1, int y1) {
	double	d, dx, dy;
	int		x, y;
	int		yi;

	dx = x1 - x0;
	dy = y1 - y0;
	yi = 1;
	if (dy < 0) {
		yi = -1;
		dy = -dy;
	}
	d = (2.0 * dy) - dx;
	y = y0;
	for (x = x0; x <= x1; x++) {
		draw_char(canvas, c, x, y);
		if (d > 0) {
			y = y + yi;
			d += (2.0 * (dy - dx));
		} else {
			d += 2.0 * dy;
		}
	}
}

static void	line_high(t_canvas *canvas, char c, int x0, int y0, int x1,
			int y1) {
	double	d, dx, dy;
	int		x, y;
	int		xi;

	dx = x1 - x0;
	dy = y1 - y0;
	xi = 1;
	if (dx < 0) {
		xi = -1;
		dx = -dx;
	}
	d = (2.0 * dx) - dy;
	x = x0;
	for (y = y0; y <= y1; y++) {
		draw_char(canvas, c, x, y);
		if (d > 0) {
			x += xi;
			d += (2.0 * (dx - dy));
		} else {
			d += 2.0 * dx;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		draw_line(t_canvas *canvas, char c, int x0, int y0, int x1,
			int y1) {
	int		diff_x, diff_y;

	diff_x = ABS(x1 - x0);
	diff_y = ABS(y1 - y0);
	if (diff_y == 0) {
		/// HORIZONTAL
		draw_horizontal_line(canvas, c, y0, x0, x1);
	} else if (diff_x == 0) {
		/// VERTICAL
		draw_vertical_line(canvas, c, x0, y0, y1);
	} else if (diff_y < diff_x) {
		/// LINE LOW
		if (x0 > x1)
			line_low(canvas, c, x1, y1, x0, y0);
		else
			line_low(canvas, c, x0, y0, x1, y1);
	} else {
		/// LINE HIGH
		if (y0 > y1)
			line_high(canvas, c, x1, y1, x0, y0);
		else
			line_high(canvas, c, x0, y0, x1, y1);
	}
}
