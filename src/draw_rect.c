/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	constrain_rect(t_canvas *canvas, int *x, int *y, int *w, int *h) {
	if (*x < 0) {
		*w += *x;
		*x = 0;		
	}
	if (*y < 0) {
		*h += *y;
		*y = 0;
	}
	if (*x + *w > canvas->width)
		*w = canvas->width - *x;
	if (*y + *h > canvas->height)
		*h = canvas->height - *y;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		draw_rect(t_canvas *canvas, char c, int rx, int ry, int rw,
			int rh) {
	int		x;

	constrain_rect(canvas, &rx, &ry, &rw, &rh);
	if (rw == 0 || rh == 0)
		return;
	/// TOP / BOTTOM LINES
	for (x = 0; x < rw; ++x) {
		canvas->array[ry][rx + x] = c;
		canvas->array[ry + rh - 1][rx + x] = c;
	}
	/// LEFT / RIGHT LINES
	if (rh > 2) {
		draw_vertical_line(canvas, c, rx, ry + 1, ry + rh - 1);
		draw_vertical_line(canvas, c, rx + rw - 1, ry + 1, ry + rh - 1);
	}
}
