/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//static	void	create_ascii_table(void) {
//	/// CREATE TABLE
//	lua_createtable(l, 0, 0);
//
//	lua_pushstring(l, "un");
//	lua_pushstring(l, "1");
//	lua_settable(l, -3);
//
//	lua_pushstring(l, "deux");
//	lua_pushstring(l, "2");
//	lua_settable(l, -3);
//
//	lua_setglobal(l, "ascii");
//
//	/// MODIFY TABLE
//	lua_getglobal(l, "ascii");
//
//	lua_pushstring(l, "un");
//	lua_pushstring(l, "3");
//	lua_settable(l, -3);
//
//	lua_setglobal(l, "ascii");
//}

static void		preinit_asciii() {
	/// CHARS
	gb_strcpy(asciii.chars.ascii, CHARS_ASCII);
	gb_memset(asciii.chars.gradient, ' ', CHARS_LEN);
	asciii.chars.set = DEFAULT_CHAR;
	/// FONT
	asciii.format.margin = 0;
	asciii.font.charspacing = 100;
	asciii.font.linespacing = 100;
	asciii.font.path = DEFAULT_FONT;
}

static void		preinit_lua(char *path) {
	/// CREATE LUA VM
	l = luaL_newstate();
	/// INIT LIBRARIES
	luaopen_base(l);
	luaopen_io(l);
	luaopen_math(l);
	luaopen_string(l);
	luaopen_table(l);
	luaopen_os(l);
	luaopen_package(l);
	luaopen_coroutine(l);
	/// LOAD MAIN FILE
	luaL_dofile(l, path);
	/// INIT SELF METATABLES
	init_meta_canvas();
	/// INIT SELF FUNCTIONS
	init_global_functions();
}

static void		preinit_raylib(void) {
	InitWindow(128, 128, "asciii");
}

static void	call_user_function(char *func) {
	lua_getglobal(l, func);
	lua_call(l, 0, 0);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		test() {
	printf("%s\n", gb_strdup("MEGA LEAKS !"));
}

int			main(void) {
	preinit_asciii();
	preinit_raylib();
	preinit_lua("main.lua");
	call_user_function("init");
	compute_font_gradient();

	printf("- - -\n");
	printf("%d %d\n", asciii.format.w, asciii.format.h);
	printf("%d %d\n", asciii.format.screen_w, asciii.format.screen_h);
	printf("%d %d %d\n", asciii.font.size, asciii.font.width, asciii.font.height);
	printf("- - -\n");

	while (WindowShouldClose() == false) {
		call_user_function("update");
		call_user_function("draw");
		record_update();
		BeginDrawing();
			render_array(asciii.canvas.base->array);
			DrawRectangle(0, 0, 75, 20, BLACK);
			DrawFPS(0, 0);
		EndDrawing();
		asciii.canvas.set = asciii.canvas.base;
	}
	CloseWindow();
	lua_close(l);
	return 0;
}
