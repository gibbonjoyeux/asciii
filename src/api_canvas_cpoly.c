/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "asciii.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			api_canvas_cpoly() {
	int		off;
	int		x, y;
	int		ext_w, ext_h, int_w, int_h;
	int		ext_vertices;
	double	angle_offset;
	char	c;

	off = get_param_canvas();
	x = get_param_int(off + 1);
	y = get_param_int(off + 2);
	ext_w = get_param_int(off + 3);
	ext_h = get_param_int(off + 4);
	int_w = get_param_int(off + 5);
	int_h = get_param_int(off + 6);
	ext_vertices = get_param_int(off + 7);
	angle_offset = get_param_float(off + 8);
	c = get_param_char(off + 9);
	draw_cpoly(asciii.canvas.active, c, x, y, ext_w, ext_h, int_w, int_h,
	ext_vertices, angle_offset);
	return 0;
}
