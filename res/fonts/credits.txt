Velvetyne: https://www.velvetyne.fr
	- tiny - Jack Halten Fahnestock: https://www.velvetyne.fr/fonts/tiny
	- steps mono - Jean-Baptiste Morizot: https://www.velvetyne.fr/fonts/steps-mono

KreativeKorp: https://www.kreativekorp.com/software/fonts/index.shtml
	- Apple II
	— Commodore 64
	- TRS-80

- amiga4ever - freaky fonts: https://www.dafont.com/amiga-forever.font
- classic console - deejayy: http://webdraft.hu/fonts/classic-console/
- pico-8 - rythmLynx & Lexaloffle: https://www.lexaloffle.com/bbs/?tid=3760 - https://drive.google.com/file/d/0B97Um39fHXlcN0xLMWxkSUdlNFE/view